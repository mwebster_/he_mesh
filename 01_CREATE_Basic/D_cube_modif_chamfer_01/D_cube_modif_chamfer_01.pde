/*
PLEASE READ INTRO TAB
 */

/////////////////////////// GLOBALS ////////////////////////////
// LIBRARY IMPORT
import wblut.math.*;
import wblut.processing.*;
import wblut.core.*;
import wblut.hemesh.*;
import wblut.geom.*;

HE_Mesh mesh;
WB_Render render;
HEC_Cube creator;

import peasy.*;
PeasyCam cam;

/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(800, 600, P3D);
  cam = new PeasyCam(this, 100);  
  creator = new HEC_Cube(); 
  creator.setEdge(60); 
  mesh = new HE_Mesh(creator);

  // SIMPLE CHAMFER MODIFIER
  HEM_ChamferCorners chamfer = new HEM_ChamferCorners().setDistance(20);
  HEM_ChamferEdges edges = new HEM_ChamferEdges().setDistance(5);
  // ADD OUR MODIFIERS TO THE MESH
  mesh.modify( chamfer ); 
  mesh.modify( edges );

  render = new WB_Render(this); // RENDER MESH
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  /*
  noStroke();
   fill(0, 0, 255);
   RENDER.drawFaces( MESH );
   */

  stroke(255, 200, 0);
  strokeWeight( 3 );
  render.drawEdges( mesh );
}

// SOME KEYS INTERACTION
void keyPressed() {

  if (key == 'e') {
    // Hemesh includes a method for exporting geometry
    // in stl file format wich is very handy for 3D printing ;–)
    //HET_Export.saveToSTL(mesh, sketchPath("export.stl"), "boo");
  }

  if (key == 's') {
    saveFrame("screenShot_###.png");
    println("screen shot taken");
  }
  if (key == 'o') {
    // reset camera origin positions  - do this before
    // exporting your shape so your shape is positioned
    // on a flat plane ready for 3D printing
    cam.reset(1000);
  }
  // Print camera position - could be helpful
  if (key == 'p') {
    float[] camPos = cam.getPosition();
    println(camPos);
  }
}