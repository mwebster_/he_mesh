/*
PLEASE READ INTRO TAB
 */

/////////////////////////// GLOBALS ////////////////////////////
// LIBRARY IMPORT
import wblut.math.*;
import wblut.processing.*;
import wblut.core.*;
import wblut.hemesh.*;
import wblut.geom.*;

// HEMESH CLASSES & OBJECTS
HE_Mesh mesh; // Our mesh object
WB_Render render; // Our render object

// CAM
import peasy.*;
PeasyCam cam;

/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(800, 600, P3D);
  smooth(8);
  cam = new PeasyCam(this, 150);  

  // OUR CREATOR 
  HEC_Cube creator = new HEC_Cube(); 

  //CREATOR PARMAMETERS
  creator.setEdge(70); // edge length in pixels

  mesh = new HE_Mesh(creator); // add our creator object to our mesh object
  render = new WB_Render(this); // RENDER object initialise
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);

  // HEMESH
  // We draw our faces using the RENDER object
  noStroke();
  fill(255, 210, 0);
  render.drawFaces( mesh ); // Draw MESH faces

  stroke(255, 0, 0);
  render.drawEdges( mesh ); // Draw MESH edges
}