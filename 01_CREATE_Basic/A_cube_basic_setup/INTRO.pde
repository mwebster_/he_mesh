/*

/////////////////////
HEMESH TUTO_2019
author: mw_2019
bitbucket: 
/////////////////////

-------------------------
Sketch : cube_basic_setup
Parent : none
-------------------------

This first sketch demonstrates how to render a cibe which 
is one of the many geometric shapes the HEC_ creator
family of classes propose. Note that we are using PeasyCam
as our 3D camera.

The process is thus :
-----------------------
  > Declare Mesh
    > Declare Creator
      > Set Creator parameters 
        > Add Creator to Mesh
          > Final Render in draw
          -----------------------


HEMESH LIBRARY AUTHOR
////////////////////////////
 http://www.wblut.com/
 http://hemesh.wblut.com/
////////////////////////////


*NB

----------------------------------------------
*/