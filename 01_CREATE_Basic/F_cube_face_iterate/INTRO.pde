/*
--------------------------------------------------------------
HEMESH TUTO_2019
author: mw_2019
bitbucket: https://bitbucket.org/mwebster_/he_mesh/src/master/
--------------------------------------------------------------


------------------------------
Sketch : cube_face_iterate
Parent : none
------------------------------

This sketch demonstrates how we can 
iterate various faces of a polygon.
We select some random faces and then extrude them.
Press 'r' to make another random selection.

The process is thus :
-----------------------
  > Declare Selection
    > Declare Face
      > Iterate faces 
        > Add faces to selection
          >  Final Render in draw
              -----------------------


HEMESH LIBRARY AUTHOR
////////////////////////////
 http://www.wblut.com/
 http://hemesh.wblut.com/
////////////////////////////


*NB

----------------------------------------------
*/