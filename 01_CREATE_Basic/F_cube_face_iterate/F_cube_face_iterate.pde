/**
 * PLEASE READ INTRO TAB
 */

/////////////////////////// GLOBALS ////////////////////////////
import wblut.math.*;
import wblut.processing.*;
import wblut.core.*;
import wblut.hemesh.*;
import wblut.geom.*;
import java.util.*;


HE_Mesh mesh; 
WB_Render render; 
// Our selection object
HE_Selection selection;

import peasy.*;
PeasyCam cam;

PFont interfaceFont;
int faceIndex = 0;
boolean isInfo = true;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(800, 600, P3D);
  smooth(8);
  cam = new PeasyCam(this, 200);  

  HEC_Cube creator = new HEC_Cube(); 
  creator.setEdge(70);
  creator.setWidthSegments(4).setHeightSegments(4).setDepthSegments(4);
  mesh = new HE_Mesh(creator);

  //Define a selection
  selection = new HE_Selection( mesh );

  //Add faces to selection
  HE_FaceIterator fItr = mesh.fItr();
  HE_Face f;
  while (fItr.hasNext ()) {
    f = fItr.next();
    selection.add(f);
  }
  render = new WB_Render(this);
  interfaceFont = createFont("Inter-Light-BETA", 24);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  noStroke();

  // mouse selection of faces
  fill(0, 255, 200);
  HE_Face fMouse=render.pickClosestFace(mesh, mouseX, mouseY);
  if (fMouse!=null) {
    render.drawFace( fMouse );
  }

  // Go through each face of selection manually
  // press 'f' to iterate
  if (faceIndex<selection.getNumberOfFaces()) {
    HE_Face fItr = selection.getFaceWithIndex(faceIndex);

    fill(0, 0, 255);
    render.drawFace(fItr);
  } else {
    faceIndex = 0;
  }

  stroke(255, 200, 0);
  strokeWeight(1);
  render.drawEdges( mesh );

  if (isInfo) {
    displayinfo();
  }
}

void keyPressed() {
  if (key == 's') {
    saveFrame("screenShot.png");
  }
  if (key == 'f') {
    faceIndex++;
  }
  
    if (key == 'i') {
    isInfo = !isInfo;
  }
}

void displayinfo() {
  cam.beginHUD();
  pushStyle();
  rectMode(CORNER);
  noStroke();
  fill(255, 33);
  rect(10, 10, 290, 160);
  String out = "";
  textFont(interfaceFont, 24);
  textSize(13);
  textAlign(LEFT);
  out += "--------- INFOS ------------\n";
  out += "fps :" + frameRate + "\n";

  out += "Keys: \n";
  out += "f - iterate faces \n";


  out += "----------------------------\n";
  pushMatrix();
  translate(35, 35);
  fill(255);
  text(out, 0, 0);
  popMatrix();
  popStyle();
  cam.endHUD();
}