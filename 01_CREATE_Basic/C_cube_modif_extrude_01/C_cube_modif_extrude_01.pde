/*
PLEASE READ INTRO TAB
 */

/////////////////////////// GLOBALS ////////////////////////////
// LIBRARY IMPORT
import wblut.math.*;
import wblut.processing.*;
import wblut.core.*;
import wblut.hemesh.*;
import wblut.geom.*;

// HEmesh CLASSES & OBJECTS
HE_Mesh mesh; // Our mesh object
WB_Render render; // Our render object

// cam
import peasy.*;
PeasyCam cam;

/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(800, 600, P3D);
  smooth(8);
  cam = new PeasyCam(this, 400);  

  // OUR CREATOR
  HEC_Cube creator = new HEC_Cube(); 

  //CREATOR PARMAMETERS
  creator.setEdge(70); 
  creator.setCenter(0, 0, 0).setZAxis(1, 1, 1).setZAngle(PI/4);
  // Activate this line to see what happens
  //creator.setWidthSegments(4).setHeightSegments(4).setDepthSegments(4); // keep these small

  mesh = new HE_Mesh(creator); // add our creator object to our mesh object

  // MODIFIER : SIMPLE EXTRUSION MODIFIER 
  HEM_Extrude extrude = new HEM_Extrude().setDistance(90);
  mesh.modify( extrude ); // ADD OUR MODIFIER TO THE mesh

  render = new WB_Render(this); 
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  // HEMESH
  noStroke();
  fill(0, 0, 255, 173);
  render.drawFaces( mesh ); // Draw mesh faces
  
  stroke(255,200,0);
  strokeWeight(3);
  render.drawEdges( mesh ); // Draw mesh edges
}

void keyPressed() {
  if (key == 's') {
     saveFrame("screenShot_###.png");
    println("screen shot taken");
  }
}