/*
--------------------------------------------------------------
HEMESH TUTO_2019
author: mw_2019
bitbucket: https://bitbucket.org/mwebster_/he_mesh/src/master/
--------------------------------------------------------------


----------------------------
Sketch : cube_modif_extrude_01
Parent : cube_basic_setup
------------------------------

This sketch demonstrates how we can implement modifications
to our shapes. 

The process is thus :
-----------------------
  > Declare Mesh object
    > Declare & instantiate Creator object
      > Set Creator parameters 
        > Add Creator to Mesh
          >  Declare Modifier
            > Set Modifier parameters
              > Add modifier to Mesh
                > Final Render in draw
                -----------------------


HEMESH LIBRARY AUTHOR
////////////////////////////
 http://www.wblut.com/
 http://hemesh.wblut.com/
////////////////////////////


*NB

----------------------------------------------
*/