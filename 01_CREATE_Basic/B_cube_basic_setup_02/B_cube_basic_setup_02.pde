/*
PLEASE READ INTRO TAB
 */

/////////////////////////// GLOBALS ////////////////////////////
// LIBRARY IMPORT
import wblut.math.*;
import wblut.processing.*;
import wblut.core.*;
import wblut.hemesh.*;
import wblut.geom.*;

// HEMESH CLASSES & OBJECTS
HE_Mesh mesh; // Our mesh object
WB_Render render; // Our render object

// CAM
import peasy.*;
PeasyCam cam;

/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(800, 600, P3D);
  smooth(8);
  cam = new PeasyCam(this, 150);  

  // OUR CREATOR 
  HEC_Cube creator = new HEC_Cube(); 

  //CREATOR PARMAMETERS
  creator.setEdge(70); // edge length in pixels

  creator.setWidthSegments(4).setHeightSegments(4).setDepthSegments(4); // keep these small

  //alternatively 
  //creator.setRadius(50);
  //creator.setInnerRadius(50);// radius of sphere inscribed in cube
  //Try these two
  //creator.setOuterRadius(25);// radius of sphere circumscribing cube
  //creator.setMidRadius(10);// radius of sphere tangential to edges

  // These params set the initial position & need to be initialised seperately 
  // from other params such as height / setWidthSegments / edges ...
  creator.setCenter(0, 0, 0).setZAxis(1, 1, 1).setZAngle(PI/4);

  mesh = new HE_Mesh(creator);

  //mesh.triangulate(); // this is one of many methods we can access from the HE_Mesh class
  render = new WB_Render(this); // RENDER object initialise
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  // HEMESH
  // We draw our faces, vertices and edges using the RENDER object
  noStroke();
  fill(255, 210, 0);
  render.drawFaces( mesh ); // Draw MESH faces

  fill(0, 200, 50);
  render.drawVertices(mesh, 2 ); // Draw MESH vertices

  stroke(255, 0, 0);
  render.drawEdges( mesh ); // Draw MESH edges
  render.drawFaceNormals( mesh, 10 );
}

void keyPressed() {
  if (key == 's') {
    saveFrame("screenShot_###.png");
    println("screen shot taken");
  }
}