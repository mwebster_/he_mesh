/*
/////////////////////
HEMESH TUTO_2019
author: mw_2019
bitbucket: 
/////////////////////

--------------------------
Sketch : cube_basic_setup_02
Parent : cube_basic_setup
----------------------------

Here we have added many more methods for our creator object.
These can give us a lot of flexibility in setting up our initial
shape by choosing not only it's size, length and dimensions but
also other geometrical data such as radius and number of segments.

The process is thus :
-----------------------
  > Declare Mesh object
    > Declare & instantiate Creator object
      > Set Creator parameters 
        > Add Creator to Mesh
          > Declare & instantiate Render object
            > Final Render
            -----------------------

HEMESH LIBRARY AUTHOR
////////////////////////////
 http://www.wblut.com/
 http://hemesh.wblut.com/
////////////////////////////


*NB

----------------------------------------------
*/