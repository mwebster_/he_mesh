/*
PLEASE READ INTRO TAB
*/

/////////////////////////// GLOBALS ////////////////////////////
// LIBRARY IMPORT
import wblut.math.*;
import wblut.processing.*;
import wblut.core.*;
import wblut.hemesh.*;
import wblut.geom.*;

HE_Mesh mesh; 
WB_Render render;

import peasy.*;
PeasyCam cam;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(800, 600, P3D);
  cam = new PeasyCam(this, 100);  
  
  HEC_Cube creator = new HEC_Cube(); 
  creator.setEdge(60);
  mesh = new HE_Mesh(creator);

  // MODIFIER
  HEM_Wireframe strut = new HEM_Wireframe();
  //Parameters for each method can be set seperately ...
  strut.setStrutRadius(100);

  // assigning 3 to setStrutFacets
  // and assigning values 50+ for setMaximumStrutOffset
  // gives odd geometries
  strut.setStrutFacets(7); // no more than 15, no less than 3
  strut.setMaximumStrutOffset(4);
  strut.setTaper(true);
  
  //mesh.triangulate();
  mesh.modify( strut );
  render = new WB_Render(this); // RENDER object initialise
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  /*
  noStroke();
  fill(0, 0, 255);
  render.drawFaces( mesh );
  */
  stroke(255, 200, 0);
  strokeWeight(2);
  render.drawEdges( mesh );

}

// SOME KEYS INTERACTION
void keyPressed() {

  if (key == 'e') {
    // Hemesh includes a method for exporting geometry
    // in stl file format wich is very handy for 3D printing ;–)
    //HET_Export.saveToSTL(MESH, sketchPath("export_###.stl"), 1.0);
  }
   if (key == 's') {
    saveFrame("screenShot_###.png");
    println("screen shot taken");
   }
   
  if (key == 'o') {
    // reset camera origin positions  - do this before
    // exporting your shape so your shape is positioned
    // on a flat plane 
    cam.reset(1000);
  }
  // Print camera position - could be helpful
  if (key == 'p') {
    float[] camPos = cam.getPosition();
    println(camPos);
  }
}