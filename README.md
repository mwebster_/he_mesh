# HE_MESH_Tutorial

[![PyPI](https://img.shields.io/pypi/l/fsfe-reuse.svg)](https://www.gnu.org/licenses/gpl-3.0.html)
[![reuse compliant](https://img.shields.io/badge/reuse-compliant-green.svg)](https://git.fsfe.org/reuse/reuse)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)


![PolyFrame_06.jpg](https://bitbucket.org/repo/jkKx8kq/images/2352175420-PolyFrame_06.jpg)
---

## Introduction

Basic introduction to Frederik Vanhoutte's HE_Mesh library for Processing. Orientated towards teaching the basic methods of 3D geometry with HE_Mesh.

## Contents

* 01_Create Basic
*
* [A brief article with more images.](https://area03.bitbucket.io/projects/polyarch.html)
* [...]

## Install

The programs in this repository can be compiled using the Processing environment along with installation of the HE_Mesh library.

* https://processing.org/
* https://github.com/wblut/HE_Mesh

## Contact & Sundries

* mark.webster[at]wanadoo.fr
* Version v0.15
* Tools used : Processing, HE_Mesh Library

## Contribute
To contribute to this project, please fork the repository and make your contribution to the
fork, then open a pull request to initiate a discussion around the contribution.

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/
